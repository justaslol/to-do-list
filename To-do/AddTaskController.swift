
import UIKit

protocol AddTask {
    func addTask(name: String)
}

class AddTaskController: UIViewController {
    
    var task: Task?
    
    var delegate: AddTask?

    @IBOutlet weak var taskNameOutlet: UITextField!
    @IBOutlet weak var addTaskTitle: UILabel!
    
    @IBAction func addAction(_ sender: Any) {
        if !taskNameOutlet.text!.isEmpty {
            delegate?.addTask(name: taskNameOutlet.text!)
            navigationController?.popViewController(animated: true)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if let task = task {
            taskNameOutlet.text = task.name
        }
    }
    
    


}

