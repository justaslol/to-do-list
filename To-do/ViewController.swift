
import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AddTask {
    
    var tasks : [Task] = []

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskCellTableViewCell
        
        cell.taskNameLabel.text = tasks[indexPath.row].name
        
        return cell
    }
    
    // Deleting a cell
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete{
            tasks.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        switch(segue.identifier ?? "") {
            
        case "AddItem":
            print("Adding a new meal.")
            let vc = segue.destination as! AddTaskController
            vc.delegate = self
            
        case "ShowDetail":
            print("showing details")
            guard let addTaskViewController = segue.destination as? AddTaskController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedTaskCell = sender as? TaskCellTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedTask = tasks[indexPath.row]
            
            addTaskViewController.delegate = self
            addTaskViewController.task = selectedTask
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
        
    }
    
    func addTask(name: String) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tasks[selectedIndexPath.row].name = name
            tableView.reloadRows(at: [selectedIndexPath], with : .none)
        } else{
            tasks.append(Task(name : name))
            tableView.reloadData()
        }
        
    }
}


